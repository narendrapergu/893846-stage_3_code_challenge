package com.cognizant.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;

import com.cognizant.models.Course;
import com.cognizant.models.Student;

public class CourseRowMapper implements RowMapper<Course>{

	@Override
	public Course mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Course course=new Course();
		
		course.setCourseId(rs.getInt(1));
		course.setTitle(rs.getString(2));
		course.setFees(rs.getFloat(3));
		course.setDescription(rs.getString(4));
		course.setTrainer(rs.getString(5));
		course.setStartDate(rs.getDate(6));
		return course;
	}

}
