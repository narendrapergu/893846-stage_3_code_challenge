package com.cognizant.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.cognizant.models.Course;
import com.cognizant.models.Student;
@Component
public interface StudentDao {
	
	public List<Course> getAllCourses();//for getting all courses from DB
	public boolean insert(Student student);//for posting student object into DB
	public boolean delete(int EnrollmentId);//for deleting students by EnrollmentId
	public Student getStudentById(int EnrollmentId);//for finding if the object already exists

}
