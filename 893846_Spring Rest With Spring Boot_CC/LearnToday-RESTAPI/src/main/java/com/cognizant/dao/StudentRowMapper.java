package com.cognizant.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.cognizant.models.Student;

public class StudentRowMapper implements RowMapper<Student>{

	@Override
	public Student mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		Student st=new Student();
		st.setEnrollmentId(rs.getInt(1));
		st.setStudentId(rs.getInt(2));
		st.setCourseId(rs.getInt(3));
		
		
		return st;
	}
	

}
