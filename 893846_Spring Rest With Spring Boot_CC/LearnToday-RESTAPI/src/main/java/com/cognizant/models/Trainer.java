package com.cognizant.models;

public class Trainer {
	
	private int trainerId;
	private String passWord;
	public int getTrainerId() {
		return trainerId;
	}
	
	public void setTrainerId(int trainerId) {
		this.trainerId = trainerId;
	}
	public String getPassWord() {
		return passWord;
	}
	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}
	

}
